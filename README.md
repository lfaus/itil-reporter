install dependencies 

`pip3 install pdfdocument python-gitlab markdown lxml beautifulsoup4 dateutil`

rename python-gitlab.cfg.example to python-gitlab.cfg

add personal access token to python-gitlab.cfg