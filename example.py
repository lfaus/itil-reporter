from io import BytesIO
from pdfdocument.document import PDFDocument

def say_hello(path):
    pdf = PDFDocument(path)
    pdf.init_report()
    pdf.h1('Hello World')
    pdf.p('Creating PDFs made easy.')
    pdf.generate()

if __name__ == "__main__":
    say_hello("example.pdf")