import gitlab
import markdown
import dateutil.parser
from pdfdocument.document import PDFDocument



def to_pdf(path, mr):
    pdf = PDFDocument(path)
    pdf.init_report()
    pdf.h1(mr.title)
    html = markdown.markdown(mr.description)
    pdf.mini_html(html)
    pdf.p("----")
    x = dateutil.parser.isoparse(mr.created_at)
    pdf.h3("Created: " + x.strftime("%B %d %H:%M:%S"))
    author = mr.author
    pdf.h3("Author: " + author['username'] + " (" + author['name'] + ")")
    pdf.p("----")
    pdf.h2("Reviewers:")
    reviewers = mr.reviewers
    for reviewer in reviewers:
        pdf.h3("Reviewer: " + reviewer['username'] + " (" + reviewer["name"] + ")")
    pdf.p("----")
    commits = mr.commits()
    print
    pdf.h2("Commits:")
    for commit in commits:
        c_date = dateutil.parser.isoparse(commit.created_at)
        pdf.p("[" + commit.short_id + "] " + commit.title + " (" + commit.author_name + ") "  + "- " + c_date.strftime("%B %d %H:%M:%S"))
    
    pdf.generate()

if __name__ == "__main__":
    gl = gitlab.Gitlab.from_config('public', ['./python-gitlab.cfg'])
    gl.auth

    project = gl.projects.get(278964)
    mr = project.mergerequests.get(100286)
    
    to_pdf("change-management.pdf", mr)


 
